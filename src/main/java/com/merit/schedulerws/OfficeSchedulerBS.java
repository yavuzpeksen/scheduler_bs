package com.merit.schedulerws;


import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OfficeSchedulerBS {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(OfficeSchedulerBS.class, args);
		Arrays.stream(ctx.getBeanDefinitionNames())
			.forEach(System.out::println);
	}
	@Bean
	CommandLineRunner runner() {
		return args -> {
			System.out.println("Command line inputs are entered here..");
			
		};
	}
}
