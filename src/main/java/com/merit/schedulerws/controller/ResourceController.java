package com.merit.schedulerws.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.merit.schedulerws.exception.CustomerNotFoundException;
import com.merit.schedulerws.view.model.Customer;

@RestController
@RequestMapping("/api/")
public class ResourceController {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceController.class);
	
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	@RequestMapping(value="customers", method = RequestMethod.GET)
	public List<Customer> getCustomers() {
		
		//Iterable<Customer> customerList = customerService.getCustomers();
		List<String> customerList = new ArrayList<>();
		customerList.add("Mehmet");
		customerList.add("Ahmet");
		
		List<Customer> custList = new ArrayList<>();
		Customer obj1 = new Customer();
		obj1.setName("Mehmet");
		obj1.setAge(40);
		Customer obj2 = new Customer();
		obj2.setName("Ahmet");
		obj2.setAge(20);
		custList.add(obj1);
		custList.add(obj2);
		return custList;
		
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="customers/add", method = RequestMethod.GET)
	public String createCustomer() {
		return "customer add operation successful";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="customers/delete/{id}", method = RequestMethod.DELETE)
	public String deleteCustomer(@PathVariable(value="id") long id) throws CustomerNotFoundException{
		
		//We assume that object is not found for deleting. We will throw Exception
		Customer customer = null;
		if(customer == null) {
			logger.error("Delete operation failed.");
			throw new CustomerNotFoundException("customer with id:" + id + " not found");
		}
		return "customer delete operation successful";
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public void handleDeleteCustomerNotFound(CustomerNotFoundException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}
	
	
	

}
