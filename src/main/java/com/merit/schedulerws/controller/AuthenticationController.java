package com.merit.schedulerws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value="/public")
public class AuthenticationController {

	  @GetMapping(value = "/login")
	  public String loginGetPage(Model model, HttpServletRequest request) {

	    return "login";
	  }
	  
	  @GetMapping(value="/system")
	  @ResponseBody
	  public SystemInformation getSystemInfo() {
		  RestTemplate restTemplate = new RestTemplate();
		  SystemInformation info = restTemplate.getForObject("http://localhost:8080/data/system.json", SystemInformation.class);
		  return info;
	  }
}
