package com.merit.schedulerws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merit.schedulerws.entity.model.User;
import com.merit.schedulerws.repository.UserRepository;

@Service
public class UserServiceImpl {

	@Autowired
	private UserRepository userReposity;
	
	public User findByUsername(String username) {
		return userReposity.findByUsername(username);
	}
	
}
