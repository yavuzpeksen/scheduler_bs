package com.merit.schedulerws.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.merit.schedulerws.entity.model.User;
import com.merit.schedulerws.service.impl.UserServiceImpl;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter{

	private static String REALM = "SCHEDULER_REALM";
	private final String ROLE_PREFIX = "ROLE_";
	
    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private UserApprovalHandler userApprovalHandler;

    @Autowired
    UserDetailsService userDetailsService;
    
    @Autowired
    UserServiceImpl userService;

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;
        
    @Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
	    clients.inMemory()
	    .withClient("scheduler-trusted-client")
            .secret(passwordEncoder().encode("secret"))
            .authorizedGrantTypes("password", "refresh_token")
            .authorities("ROLE_USER", "ROLE_ADMIN")
            .scopes("read", "write", "trust")
            .accessTokenValiditySeconds(300)
            .refreshTokenValiditySeconds(600)
            .and()
            .withClient("scheduler-client")
            .secret(passwordEncoder().encode("secret"))
            .authorizedGrantTypes("password", "refresh_token")
            .authorities("ROLE_USER")
            .scopes("read")
            .accessTokenValiditySeconds(300)
            .refreshTokenValiditySeconds(600);
	}

    
    @Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    	endpoints.tokenStore(tokenStore).userApprovalHandler(userApprovalHandler)
    	.authenticationManager(new AuthenticationManager() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                String username = authentication.getPrincipal() + "";
                String password = authentication.getCredentials() + "";
                User user = userService.findByUsername(username);
                if(user == null || user.isDeleted())
                	throw new BadCredentialsException("1000");
                if(!user.isActive())
                	throw new DisabledException("1001");
                if(!passwordEncoder().matches(password, user.getPassword()))
                	throw new BadCredentialsException("1000");
                
                List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
                authorityList.add(new SimpleGrantedAuthority(ROLE_PREFIX + user.getAuthorityType()));
                
                //authorityList.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
                //authorityList.add(new SimpleGrantedAuthority("ROLE_TRUSTED_CLIENT"));
                return new UsernamePasswordAuthenticationToken(username, password, authorityList);
            }
        }).addInterceptor(new HandlerInterceptorAdapter() {
    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse rs, Object o) throws Exception {
        rs.setHeader("Access-Control-Allow-Origin", "*");
        rs.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
        rs.setHeader("Access-Control-Max-Age", "3600");
        rs.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        return true;
    }
});
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.realm(REALM);
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
		Map<String, PasswordEncoder> encoders = new HashMap<>();
		encoders.put("bcrypt", new BCryptPasswordEncoder());
		
		PasswordEncoder passwordEncoder =
			    new DelegatingPasswordEncoder("bcrypt", encoders);
		
        return passwordEncoder;
    }
}
